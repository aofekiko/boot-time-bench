#!/bin/bash
set -e

function cleanup {
  virtctl stop ${VM_NAME}
}

VM_NAME=***
VM_IP=***
LOOPS=20

trap cleanup ERR
for i in {1..$LOOPS}; do

  until virtctl start ${VM_NAME}; do
    sleep 1
  done
  CREATION_TIMESTAMP=$(oc get vmi bastion -o json | jq '.metadata.creationTimestamp' | tr -d '"' | sed -E 's/.T(.)Z/\1/')
  until ssh -o ConnectTimeout=10 -o BatchMode=yes -o StrictHostKeyChecking=no ${VM_IP} exit; do 
    sleep 1
  done
  BOOT_TIME=$(ssh -o ConnectTimeout=10 -o BatchMode=yes -o StrictHostKeyChecking=no ${VM_IP} journalctl | grep "Reached target Default" | tail -n1 | awk '{print $3}')
  echo "boot: $BOOT_TIME"
  while [ -z "$BOOT_TIME" ]; do
    sleep 1
    BOOT_TIME=$(ssh -o ConnectTimeout=10 -o BatchMode=yes -o StrictHostKeyChecking=no ${VM_IP} journalctl | grep "Reached target Default" | tail -n1 | awk '{print $3}')
    echo "boot retry: $BOOT_TIME"
  done
  echo "${CREATION_TIMESTAMP},${BOOT_TIME}" >> disk_output
  virtctl stop ${VM_NAME}
  sleep 10
done
